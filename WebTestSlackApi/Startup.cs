﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebTestSlackApi.Startup))]
namespace WebTestSlackApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
