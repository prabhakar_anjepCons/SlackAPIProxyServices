﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlackApiConsole.Model
{
   public class Channels
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool is_channel { get; set; }
        public int created { get; set; }
        public string creator { get; set; }
        public bool is_archived { get; set; }
        public bool is_general { get; set; }
        public string name_normalized { get; set; }
        public bool is_shared { get; set; }
        public bool is_org_shared { get; set; }
        public bool is_member { get; set; }
        public string last_read { get; set; }
        public string latest { get; set; }
        public int unread_count { get; set; }
        public int unread_count_display { get; set; }
        public List<members> _members { get; set; }
        public List<topic> _topic { get; set; }
        public List<purpose> _purpose { get; set; }
        public List<previous_names> _previous_names { get; set; }
    }
    public class members
    {
        public string  Name { get; set; }
    }
    public class topic
    {
        public string  value { get; set; }
        public string creator { get; set; }
        public int last_set { get; set; }
    }
    public class purpose
    {
        public string value { get; set; }
        public string creator { get; set; }
        public int last_set { get; set; }
    }
    public class previous_names
    {
        public int num_members { get; set; }
    }
}
