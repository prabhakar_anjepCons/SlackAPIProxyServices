﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SlackApiConsole.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace SlackApiConsole
{
   public class TestChannels
    {
        public XmlDocument GetInboxOutboxSms(string url)
        {
            try
            {               
                WebRequest request = WebRequest.Create(url);
                // Get the response.
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                //dynamic data = JObject.Parse(responseFromServer);
                //  dt = (DataTable)JsonConvert.DeserializeObject(responseFromServer, (typeof(DataTable)));
                //XmlDocument doc = new XmlDocument();
                XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(responseFromServer, "ok");               
                return doc;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable JsonStringToDataTable(string jsonString)
        {
            Channels _Channelsobj = new Channels();
            List<Channels> _lstChannels = new List<Model.Channels>();
            DataTable dt = new DataTable();
            int startlength = jsonString.IndexOf("[");         
            jsonString = jsonString.Remove(0, startlength);
            int Lastlenght = jsonString.LastIndexOf("]");
            Lastlenght = Lastlenght + 1;
            int totallenght = jsonString.Length;
            jsonString = jsonString.Remove(Lastlenght, totallenght - Lastlenght);
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Trim().Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        //throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                if (!dt.Columns.Contains(AddColumnName.Trim()))
                {
                    dt.Columns.Add(AddColumnName.Trim());
                }
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns.Trim()] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }
    }
}
