﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackdnd" in both code and config file together.
    [ServiceContract]
    public interface ISlackdnd
    {
        [OperationContract]
        string dndendDnd();
        [OperationContract]
        string dndendSnooze();
        [OperationContract]
        string dndinfo(dndinfoCl _dndinfoCl);
        [OperationContract]
        string dndsetSnooze(dndsetSnoozeCl _dndsetSnoozeCl);
        [OperationContract]
        string dndteamInfo(dndinfoCl _dndinfoCl);
    }

    #region Model Declaration
    public class dndinfoCl
    {
        [DataMember(IsRequired = true)]
        public string user { get; set; }     
    }

    public class dndsetSnoozeCl
    {
        [DataMember(IsRequired = true)]
        public string num_minutes { get; set; }
    }
    #endregion
}
