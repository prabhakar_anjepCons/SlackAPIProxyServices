﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackTeamProfile" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackTeamProfile.svc or SlackTeamProfile.svc.cs at the Solution Explorer and start debugging.
    public class SlackTeamProfile : ISlackTeamProfile
    {
        public string teamprofileget(teamprofilegetCl _teamprofilegetCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "team.profile.get?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_teamprofilegetCl.visibility != "")
                ApiUrl += "&visibility=" + _teamprofilegetCl.visibility;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
