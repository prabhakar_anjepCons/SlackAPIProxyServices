﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackSearch" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackSearch.svc or SlackSearch.svc.cs at the Solution Explorer and start debugging.
    public class SlackSearch : ISlackSearch
    {
        public string searchall(SearchAllCl _SearchAllCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "search.all?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&query=" + _SearchAllCl.query;
                if(_SearchAllCl.count != "")
                    ApiUrl += "&count=" + _SearchAllCl.count;
                if (_SearchAllCl.highlight != "")
                    ApiUrl += "&highlight=" + _SearchAllCl.highlight;
                if (_SearchAllCl.page != "")
                    ApiUrl += "&page=" + _SearchAllCl.page;
                if (_SearchAllCl.sort != "")
                    ApiUrl += "&sort=" + _SearchAllCl.sort;
                if (_SearchAllCl.sort_dir != "")
                    ApiUrl += "&sort_dir=" + _SearchAllCl.sort_dir;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string searchfiles(searchfilesCl _searchfilesCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "search.files?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&query=" + _searchfilesCl.query;
                if (_searchfilesCl.count != "")
                    ApiUrl += "&count=" + _searchfilesCl.count;
                if (_searchfilesCl.highlight != "")
                    ApiUrl += "&highlight=" + _searchfilesCl.highlight;
                if (_searchfilesCl.page != "")
                    ApiUrl += "&page=" + _searchfilesCl.page;
                if (_searchfilesCl.sort != "")
                    ApiUrl += "&sort=" + _searchfilesCl.sort;
                if (_searchfilesCl.sort_dir != "")
                    ApiUrl += "&sort_dir=" + _searchfilesCl.sort_dir;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string searchmessages(searchmessagesCl _searchmessagesCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "search.messages?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&query=" + _searchmessagesCl.query;
                if (_searchmessagesCl.count != "")
                    ApiUrl += "&count=" + _searchmessagesCl.count;
                if (_searchmessagesCl.highlight != "")
                    ApiUrl += "&highlight=" + _searchmessagesCl.highlight;
                if (_searchmessagesCl.page != "")
                    ApiUrl += "&page=" + _searchmessagesCl.page;
                if (_searchmessagesCl.sort != "")
                    ApiUrl += "&sort=" + _searchmessagesCl.sort;
                if (_searchmessagesCl.sort_dir != "")
                    ApiUrl += "&sort_dir=" + _searchmessagesCl.sort_dir;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
