﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackReactions" in both code and config file together.
    [ServiceContract]
    public interface ISlackReactions
    {
        [OperationContract]
        string reactionsadd(reactionsaddCl _reactionsaddCl);
        [OperationContract]
        string reactionsget(reactionsgetCl _reactionsgetCl);
        [OperationContract]
        string reactionslist(reactionslistCl _reactionslistCl);
        [OperationContract]
        string reactionsremove(reactionsremoveCl _reactionsremoveCl);
    }
    public class reactionsaddCl
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    public class reactionsgetCl
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string full { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    public class reactionslistCl
    {
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string full { get; set; }
        [DataMember]
        public string page { get; set; }
        [DataMember]
        public string user { get; set; }      
    }
    public class reactionsremoveCl
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
}
