﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Slackgroups" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Slackgroups.svc or Slackgroups.svc.cs at the Solution Explorer and start debugging.
    public class Slackgroups : ISlackgroups
    {
        public string groupsarchive(groupsarchiveCl _groupsarchiveCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.archive?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsarchiveCl.channel;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsclose(groupscloseCl _groupscloseCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.close?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupscloseCl.channel;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupscreate(groupscreateCl _groupscreateCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.close?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&name=" + _groupscreateCl.name;
                if(_groupscreateCl.validate != "")
                ApiUrl += "&validate=" + _groupscreateCl.validate;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupscreateChild(groupscreateChildCl _groupscreateChildCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.createChild?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupscreateChildCl.channel;              
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupshistory(groupshistorycl _groupshistorycl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.history?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupshistorycl.channel;
                if(_groupshistorycl.latest != "")
                    ApiUrl += "&latest=" + _groupshistorycl.latest;
                if (_groupshistorycl.oldest != "")
                    ApiUrl += "&oldest=" + _groupshistorycl.oldest;
                if (_groupshistorycl.inclusive != "")
                    ApiUrl += "&inclusive=" + _groupshistorycl.inclusive;
                if (_groupshistorycl.count != "")
                    ApiUrl += "&count=" + _groupshistorycl.count;
                if (_groupshistorycl.unreads != "")
                    ApiUrl += "&unreads=" + _groupshistorycl.unreads;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsinfo(groupsinfocl _groupshistorycl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.info?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupshistorycl.channel;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsinvite(groupsinvitecl _groupsinvitecl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.invite?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsinvitecl.channel;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupskick(groupskickcl _groupskickcl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.kick?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupskickcl.channel;
                ApiUrl += "&user=" + _groupskickcl.user;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsleave(groupsleavecl _groupsleavecl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.leave?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsleavecl.channel;            
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupslist(groupslistcl _groupslistcl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.list?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_groupslistcl.exclude_archived != "")
                ApiUrl += "&exclude_archived=" + _groupslistcl.exclude_archived;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string groupsmark(groupsmarkcl _groupsmarkcl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.mark?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsmarkcl.channel;
                ApiUrl += "&ts=" + _groupsmarkcl.ts;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsopen(groupsopencl _groupsopencl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.open?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsopencl.channel;              
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsrename(groupsrenamecl _groupsrenamecl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.open?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsrenamecl.channel;
                ApiUrl += "&name=" + _groupsrenamecl.name;
                if(_groupsrenamecl.validate != "")
                ApiUrl += "&validate=" + _groupsrenamecl.validate;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsreplies(groupsrepliescl _groupsrepliescl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.replies?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsrepliescl.channel;
                ApiUrl += "&thread_ts=" + _groupsrepliescl.thread_ts;             
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupssetPurpose(groupssetPurposecl _groupssetPurposecl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.setPurpose?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupssetPurposecl.channel;
                ApiUrl += "&purpose=" + _groupssetPurposecl.purpose;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string groupsunarchive(groupsunarchivecl _groupsunarchivecl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "groups.unarchive?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _groupsunarchivecl.channel;                
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
