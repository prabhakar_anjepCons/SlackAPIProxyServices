﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackTeamProfile" in both code and config file together.
    [ServiceContract]
    public interface ISlackTeamProfile
    {
        [OperationContract]
        string teamprofileget(teamprofilegetCl _teamprofilegetCl);
    }
    #region Model Declaration
    public class teamprofilegetCl
    {
        [DataMember]
        public string visibility { get; set; }
    }
    #endregion
}
