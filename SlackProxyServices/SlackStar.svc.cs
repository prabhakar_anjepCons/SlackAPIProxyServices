﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackStar" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackStar.svc or SlackStar.svc.cs at the Solution Explorer and start debugging.
    public class SlackStar : ISlackStar
    {
        public string starsadd(starsaddCl _starsaddCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "stars.add?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_starsaddCl.channel != "")
                    ApiUrl += "&channel=" + _starsaddCl.channel;
                if (_starsaddCl.file != "")
                    ApiUrl += "&file=" + _starsaddCl.file;
                if (_starsaddCl.file_comment != "")
                    ApiUrl += "&file_comment=" + _starsaddCl.file_comment;
                if (_starsaddCl.timestamp != "")
                    ApiUrl += "&timestamp=" + _starsaddCl.timestamp;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string starslist(starslistCl _starslistCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "stars.add?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if (_starslistCl.count != "")
                    ApiUrl += "&count=" + _starslistCl.count;
                if (_starslistCl.page != "")
                    ApiUrl += "&page=" + _starslistCl.page;              
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string starsremove(starsremoveCl _starsremoveCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "stars.remove?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                 if(_starsremoveCl.channel != "")
                    ApiUrl += "&channel=" + _starsremoveCl.channel;
                if (_starsremoveCl.file != "")
                    ApiUrl += "&file=" + _starsremoveCl.file;
                if (_starsremoveCl.file_comment != "")
                    ApiUrl += "&file_comment=" + _starsremoveCl.file_comment;
                if (_starsremoveCl.timestamp != "")
                    ApiUrl += "&timestamp	=" + _starsremoveCl.timestamp;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
