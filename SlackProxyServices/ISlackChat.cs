﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackChat" in both code and config file together.
    [ServiceContract]
    public interface ISlackChat
    {
        [OperationContract]
        string chatdelete(chatdeleteCl _chatdeleteCl);

        [OperationContract]
        string chatmeMessage(chatmeMessageCl _chatmeMessageCl);

        [OperationContract]
        string chatpostMessage(chatpostMessageCl _chatpostMessageCl);

        [OperationContract]
        string chatunfurl(chatunfurlCl _chatunfurlCl);

        [OperationContract]
        string chatupdate(chatupdateCl _chatupdateCl);
    }


    #region Model Declaration
    public class chatdeleteCl
    {
        [DataMember(IsRequired = true)]
        public string ts { get; set; }

        [DataMember(IsRequired = true)]
        public string channel { get; set; }

        [DataMember(IsRequired = true)]
        public string as_user { get; set; }
    }

    public class chatmeMessageCl
    {       
        [DataMember(IsRequired = true)]
        public string channel { get; set; }

        [DataMember(IsRequired = true)]
        public string text { get; set; }
    }

    public class chatpostMessageCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember(IsRequired = true)]
        public string text { get; set; }
        [DataMember]
        public string parse { get; set; }
        [DataMember]
        public string link_names { get; set; }
        [DataMember]
        public string attachments { get; set; }
        [DataMember]
        public string unfurl_links { get; set; }
        [DataMember]
        public string unfurl_media { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string as_user { get; set; }
        [DataMember]
        public string icon_url { get; set; }
        [DataMember]
        public string icon_emoji { get; set; }
        [DataMember]
        public string thread_ts { get; set; }
        [DataMember]
        public string reply_broadcast { get; set; }
    }
    public class chatunfurlCl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string ts { get; set; }
        [DataMember]
        public string unfurls { get; set; }
        [DataMember]
        public string user_auth_required { get; set; }
    }

    public class chatupdateCl
    {
        [DataMember]
        public string ts { get; set; }
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string attachments { get; set; }
        [DataMember]
        public string parse { get; set; }
        [DataMember]
        public string link_names { get; set; }
        [DataMember]
        public string as_user { get; set; }
    }
    #endregion
}
