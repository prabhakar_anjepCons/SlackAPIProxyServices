﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackRtm" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackRtm.svc or SlackRtm.svc.cs at the Solution Explorer and start debugging.
    public class SlackRtm : ISlackRtm
    {
        public string searchall(RtmconnectCl _RtmconnectCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "rtm.connect?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();              
                if (_RtmconnectCl.batch_presence_aware != "")
                    ApiUrl += "&batch_presence_aware=" + _RtmconnectCl.batch_presence_aware;
                if (_RtmconnectCl.presence_sub != "")
                    ApiUrl += "&presence_sub=" + _RtmconnectCl.presence_sub;               
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string start(startCl _startCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "rtm.start?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if (_startCl.batch_presence_aware != "")
                    ApiUrl += "&batch_presence_aware=" + _startCl.batch_presence_aware;
                if (_startCl.mpim_aware != "")
                    ApiUrl += "&mpim_aware=" + _startCl.mpim_aware;
                if (_startCl.no_latest != "")
                    ApiUrl += "&no_latest=" + _startCl.no_latest;
                if (_startCl.no_unreads != "")
                    ApiUrl += "&no_unreads=" + _startCl.no_unreads;
                if (_startCl.presence_sub != "")
                    ApiUrl += "&presence_sub=" + _startCl.presence_sub;
                if (_startCl.simple_latest != "")
                    ApiUrl += "&simple_latest=" + _startCl.simple_latest;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
