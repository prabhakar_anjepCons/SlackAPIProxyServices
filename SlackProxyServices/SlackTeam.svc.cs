﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackTeam" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackTeam.svc or SlackTeam.svc.cs at the Solution Explorer and start debugging.
    public class SlackTeam : ISlackTeam
    {
        public string teamaccessLogs(teamaccessLogsCl _teamaccessLogsCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "team.accessLogs?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_teamaccessLogsCl.before != "")
                ApiUrl += "&before=" + _teamaccessLogsCl.before;
                if (_teamaccessLogsCl.count != "")
                    ApiUrl += "&count=" + _teamaccessLogsCl.count;
                if (_teamaccessLogsCl.page != "")
                    ApiUrl += "&page=" + _teamaccessLogsCl.page;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string teambillableInfo(teambillableInfoCl _teambillableInfoCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "team.billableInfo?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if (_teambillableInfoCl.user != "")
                    ApiUrl += "&user=" + _teambillableInfoCl.user;              
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string teaminfo()
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "team.info?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();              
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string teamintegrationLogs(teamintegrationLogsCl _teamintegrationLogsCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "team.integrationLogs?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_teamintegrationLogsCl.app_id != "")
                    ApiUrl += "&app_id=" + _teamintegrationLogsCl.app_id;
                if (_teamintegrationLogsCl.change_type != "")
                    ApiUrl += "&change_type=" + _teamintegrationLogsCl.change_type;
                if (_teamintegrationLogsCl.count != "")
                    ApiUrl += "&count=" + _teamintegrationLogsCl.count;
                if (_teamintegrationLogsCl.page != "")
                    ApiUrl += "&page=" + _teamintegrationLogsCl.page;
                if (_teamintegrationLogsCl.service_id != "")
                    ApiUrl += "&service_id=" + _teamintegrationLogsCl.service_id;
                if (_teamintegrationLogsCl.user != "")
                    ApiUrl += "&user=" + _teamintegrationLogsCl.user;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
