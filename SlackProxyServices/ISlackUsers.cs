﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackUsers" in both code and config file together.
    [ServiceContract]
    public interface ISlackUsers
    {
        [OperationContract]
        string usersdeletePhoto();
        [OperationContract]
        string usersgetPresence(usersgetPresenceCl _usersgetPresenceCl);
        [OperationContract]
        string usersidentity();
        [OperationContract]
        string usersinfo(usersinfoCl _usersinfoCl);
        [OperationContract]
        string userslist(userslistCl _userslistCl);
        [OperationContract]
        string userssetActive();
        [OperationContract]
        string userssetPresence(userssetPresenceCl _userssetPresenceCl);
        [OperationContract]
        string usersprofileget(usersprofilegetCl _usersprofilegetCl);
    }
    #region Model Declaration
    public class usersgetPresenceCl
    {
        [DataMember(IsRequired = true)]
        public string user { get; set; }     
    }
    public class usersinfoCl
    {
        [DataMember(IsRequired = true)]
        public string user { get; set; }
    }
    public class userslistCl
    {
        [DataMember]
        public string cursor { get; set; }
        [DataMember]
        public string limit { get; set; }
        [DataMember]
        public string presence { get; set; }
    }
    public class userssetPresenceCl
    {
        [DataMember]
        public string presence { get; set; }
    }
    public class usersprofilegetCl
    {
        [DataMember]
        public string include_labels { get; set; }
        [DataMember]
        public string user { get; set; }
    }
    #endregion
}
