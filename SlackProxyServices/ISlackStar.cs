﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackStar" in both code and config file together.
    [ServiceContract]
    public interface ISlackStar
    {
        [OperationContract]
        string starsadd(starsaddCl _starsaddCl);
        [OperationContract]
        string starslist(starslistCl _starslistCl);
        [OperationContract]
        string starsremove(starsremoveCl _starsremoveCl);
    }
    #region Model Declaration
    public class starsaddCl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    public class starslistCl
    {
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string page { get; set; }      
    }
    public class starsremoveCl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    #endregion
}
