﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackFiles" in both code and config file together.
    [ServiceContract]
    public interface ISlackFiles
    {
        [OperationContract]
        string filesdelete(filescommentsaddCl _filescommentsaddCl);

        [OperationContract]
        string filesinfo(filesinfoCl _filesinfoCl);

        [OperationContract]
        string fileslist(fileslistCl _fileslistCl);

        [OperationContract]
        string filesrevokePublicURL(filesrevokePublicURLCl _filesrevokePublicURLCl);

        [OperationContract]
        string filessharedPublicURL(filessharedPublicURLCl _filessharedPublicURLCl);

        [OperationContract]
        string filesupload(filesuploadCl _filesuploadCl);
    }

    #region Model Declaration
    public class filesdeleteCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }    
    }

    public class filesinfoCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string page { get; set; }
    }

    public class fileslistCl
    {
        [DataMember]
        public string user { get; set; }
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string ts_from { get; set; }
        [DataMember]
        public string ts_to { get; set; }
        [DataMember]
        public string types { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string page { get; set; }
    }
    public class filesrevokePublicURLCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }
    }
    public class filessharedPublicURLCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }
    }
    public class filesuploadCl
    {
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string content { get; set; }
        [DataMember]
        public string filetype { get; set; }
        [DataMember]
        public string filename { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string initial_comment { get; set; }
        [DataMember]
        public string channels { get; set; }
    }
    #endregion
}
