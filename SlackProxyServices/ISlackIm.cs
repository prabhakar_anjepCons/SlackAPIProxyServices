﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackIm" in both code and config file together.
    [ServiceContract]
    public interface ISlackIm
    {
        [OperationContract]
        string imclose(imcloseCl _imcloseCl);
        [OperationContract]
        string imhistory(imhistoryCl _imhistoryCl);
        [OperationContract]
        string imlist();
        [OperationContract]
        string immark(immarkCl _immarkCl);
        [OperationContract]
        string imopen(imopenCl _imopenCl);
        [OperationContract]
        string imreplies(imrepliesCl _imrepliesCl);
    }
    #region Model Declaration
    public class imcloseCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }       
    }
    public class imhistoryCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        public string count { get; set; }
        public string inclusive { get; set; }
        public string latest { get; set; }
        public string oldest { get; set; }
        public string unreads { get; set; }
    }

    public class immarkCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string ts { get; set; }
    }

    public class imopenCl
    {
        [DataMember(IsRequired = true)]
        public string user { get; set; }
        [DataMember]
        public string return_im { get; set; }
    }

    public class imrepliesCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string thread_ts { get; set; }
    }
    #endregion
}
