﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackRtm" in both code and config file together.
    [ServiceContract]
    public interface ISlackRtm
    {
        [OperationContract]
        string searchall(RtmconnectCl _RtmconnectCl);
        [OperationContract]
        string start(startCl _startCl);
    }   
    public class RtmconnectCl
    {     
        [DataMember]
        public string batch_presence_aware { get; set; }
        [DataMember]
        public string presence_sub { get; set; }       
    }
    public class startCl
    {
        [DataMember]
        public string batch_presence_aware { get; set; }
        [DataMember]
        public string mpim_aware { get; set; }
        [DataMember]
        public string no_latest { get; set; }
        [DataMember]
        public string no_unreads { get; set; }
        [DataMember]
        public string presence_sub { get; set; }
        [DataMember]
        public string simple_latest { get; set; }
    }
}
