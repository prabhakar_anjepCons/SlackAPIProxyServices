﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace SlackProxyServices
{
    public class WebRequestCl
    {
        public static string ReturnWebRequest(string APiUrl)
        {
            WebRequest request = WebRequest.Create(APiUrl);
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            //dynamic data = JObject.Parse(responseFromServer);
            //  dt = (DataTable)JsonConvert.DeserializeObject(responseFromServer, (typeof(DataTable)));
            //XmlDocument doc = new XmlDocument();
            //XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(responseFromServer, "ok");
            return responseFromServer;
        }
    }
}