﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackReactions" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackReactions.svc or SlackReactions.svc.cs at the Solution Explorer and start debugging.
    public class SlackReactions : ISlackReactions
    {
        public string reactionsadd(reactionsaddCl _reactionsaddCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "reactions.add?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&name=" + _reactionsaddCl.name;
                if (_reactionsaddCl.channel != "")
                    ApiUrl += "&channel=" + _reactionsaddCl.channel;
                if (_reactionsaddCl.file != "")
                    ApiUrl += "&file=" + _reactionsaddCl.file;
                if (_reactionsaddCl.file_comment != "")
                    ApiUrl += "&file_comment=" + _reactionsaddCl.file_comment;
                if (_reactionsaddCl.timestamp != "")
                    ApiUrl += "&timestamp=" + _reactionsaddCl.timestamp;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string reactionsget(reactionsgetCl _reactionsgetCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "reactions.get?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();               
                if (_reactionsgetCl.channel != "")
                    ApiUrl += "&channel=" + _reactionsgetCl.channel;
                if (_reactionsgetCl.file != "")
                    ApiUrl += "&file=" + _reactionsgetCl.file;
                if (_reactionsgetCl.file_comment != "")
                    ApiUrl += "&file_comment=" + _reactionsgetCl.file_comment;
                if (_reactionsgetCl.full != "")
                    ApiUrl += "&full=" + _reactionsgetCl.full;
                if (_reactionsgetCl.timestamp != "")
                    ApiUrl += "&timestamp=" + _reactionsgetCl.timestamp;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string reactionslist(reactionslistCl _reactionslistCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "reactions.list?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if (_reactionslistCl.count != "")
                    ApiUrl += "&count=" + _reactionslistCl.count;
                if (_reactionslistCl.full != "")
                    ApiUrl += "&full=" + _reactionslistCl.full;
                if (_reactionslistCl.page != "")
                    ApiUrl += "&page=" + _reactionslistCl.page;
                if (_reactionslistCl.user != "")
                    ApiUrl += "&user=" + _reactionslistCl.user;            
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string reactionsremove(reactionsremoveCl _reactionsremoveCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "reactions.remove?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&name=" + _reactionsremoveCl.name;
                if (_reactionsremoveCl.channel != "")
                    ApiUrl += "&channel=" + _reactionsremoveCl.channel;
                if (_reactionsremoveCl.file != "")
                    ApiUrl += "&file=" + _reactionsremoveCl.file;
                if (_reactionsremoveCl.file_comment != "")
                    ApiUrl += "&file_comment=" + _reactionsremoveCl.file_comment;
                if (_reactionsremoveCl.timestamp != "")
                    ApiUrl += "&timestamp=" + _reactionsremoveCl.timestamp;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
