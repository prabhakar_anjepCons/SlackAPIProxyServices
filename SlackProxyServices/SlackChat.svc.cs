﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackChat" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackChat.svc or SlackChat.svc.cs at the Solution Explorer and start debugging.
    public class SlackChat : ISlackChat
    {
        public string chatdelete(chatdeleteCl _chatdeleteCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "chat.delete?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&ts=" + _chatdeleteCl.ts;
                ApiUrl += "&channel=" + _chatdeleteCl.channel;
                ApiUrl += "&as_user=" + _chatdeleteCl.as_user;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string chatmeMessage(chatmeMessageCl _chatmeMessageCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "chat.meMessage?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();              
                ApiUrl += "&channel=" + _chatmeMessageCl.channel;
                ApiUrl += "&text=" + _chatmeMessageCl.text;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string chatpostMessage(chatpostMessageCl _chatpostMessageCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "chat.postMessage?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                //Required
                ApiUrl += "&channel=" + _chatpostMessageCl.channel;
                //Required
                ApiUrl += "&text=" + _chatpostMessageCl.text;
                //Optional
                if(_chatpostMessageCl.parse != "")
                ApiUrl += "&parse=" + _chatpostMessageCl.parse;
                //Optional
                if (_chatpostMessageCl.link_names != "")
                    ApiUrl += "&link_names=" + _chatpostMessageCl.link_names;
                //Optional
                if (_chatpostMessageCl.attachments != "")
                    ApiUrl += "&attachments=" + _chatpostMessageCl.attachments;
                //Optional
                if (_chatpostMessageCl.unfurl_links != "")
                    ApiUrl += "&unfurl_links=" + _chatpostMessageCl.unfurl_links;
                //Optional
                if (_chatpostMessageCl.unfurl_media != "")
                    ApiUrl += "&unfurl_media=" + _chatpostMessageCl.unfurl_media;
                //Optional
                if (_chatpostMessageCl.username != "")
                    ApiUrl += "&username=" + _chatpostMessageCl.username;
                //Optional
                if (_chatpostMessageCl.as_user != "")
                    ApiUrl += "&as_user=" + _chatpostMessageCl.as_user;
                //Optional
                if (_chatpostMessageCl.icon_url != "")
                    ApiUrl += "&icon_url=" + _chatpostMessageCl.icon_url;
                //Optional
                if (_chatpostMessageCl.icon_emoji != "")
                    ApiUrl += "&icon_emoji=" + _chatpostMessageCl.icon_emoji;
                //Optional
                if (_chatpostMessageCl.thread_ts != "")
                    ApiUrl += "&thread_ts=" + _chatpostMessageCl.thread_ts;
                //Optional
                if (_chatpostMessageCl.reply_broadcast != "")
                    ApiUrl += "&reply_broadcast=" + _chatpostMessageCl.reply_broadcast;

                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string chatunfurl(chatunfurlCl _chatunfurlCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "chat.unfurl?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _chatunfurlCl.channel;
                ApiUrl += "&ts=" + _chatunfurlCl.channel;
                ApiUrl += "&unfurls=" + _chatunfurlCl.unfurls;
                if(_chatunfurlCl.user_auth_required != "")
                ApiUrl += "&unfurls=" + _chatunfurlCl.user_auth_required;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string chatupdate(chatupdateCl _chatupdateCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "chat.update?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                //Required
                ApiUrl += "&ts=" + _chatupdateCl.ts;
                //Required
                ApiUrl += "&channel=" + _chatupdateCl.channel;
                //Required
                ApiUrl += "&text=" + _chatupdateCl.text;
                //Optional
                if(_chatupdateCl.attachments != "")
                ApiUrl += "&attachments=" + _chatupdateCl.attachments;
                //Optional
                if (_chatupdateCl.parse != "")
                    ApiUrl += "&parse=" + _chatupdateCl.parse;
                //Optional
                if (_chatupdateCl.link_names != "")
                    ApiUrl += "&link_names=" + _chatupdateCl.link_names;
                //Optional
                if (_chatupdateCl.as_user != "")
                    ApiUrl += "&as_user=" + _chatupdateCl.as_user;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
 }

