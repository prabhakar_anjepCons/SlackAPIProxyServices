﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackReminders" in both code and config file together.
    [ServiceContract]
    public interface ISlackReminders
    {
        [OperationContract]
        string remindersadd(remindersaddCl _remindersaddCl);
        [OperationContract]
        string reminderscomplete(reminderscompleteCl _reminderscompleteCl);
        [OperationContract]
        string remindersdelete(remindersdeleteCl _remindersdeleteCl);
        [OperationContract]
        string remindersinfo(remindersinfoCl _remindersinfoCl);
        [OperationContract]
        string reminderslist();
    }
    public class remindersaddCl
    {
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string time { get; set; }
        [DataMember]
        public string user { get; set; }
    }
    public class reminderscompleteCl
    {
        [DataMember]
        public string reminder { get; set; }       
    }
    public class remindersdeleteCl
    {
        [DataMember]
        public string reminder { get; set; }
    }
    public class remindersinfoCl
    {
        [DataMember]
        public string reminder { get; set; }
    }
}
