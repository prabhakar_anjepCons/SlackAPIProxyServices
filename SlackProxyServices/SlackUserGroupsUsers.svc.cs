﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackUserGroupsUsers" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackUserGroupsUsers.svc or SlackUserGroupsUsers.svc.cs at the Solution Explorer and start debugging.
    public class SlackUserGroupsUsers : ISlackUserGroupsUsers
    {
        public string usergroupsuserslist(usergroupsuserslistCl _usergroupsuserslistCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.disable?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&usergroup=" + _usergroupsuserslistCl.usergroup;
                if (_usergroupsuserslistCl.include_disabled != "")
                    ApiUrl += "&include_disabled=" + _usergroupsuserslistCl.include_disabled;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string usergroupsusersupdate(usergroupsusersupdateCl _usergroupsusersupdateCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.disable?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if (_usergroupsusersupdateCl.usergroup != "")
                    ApiUrl += "&usergroup=" + _usergroupsusersupdateCl.usergroup;
                if (_usergroupsusersupdateCl.users != "")
                    ApiUrl += "&users=" + _usergroupsusersupdateCl.users;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
