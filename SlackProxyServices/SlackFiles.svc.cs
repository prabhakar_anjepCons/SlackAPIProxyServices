﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackFiles" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackFiles.svc or SlackFiles.svc.cs at the Solution Explorer and start debugging.
    public class SlackFiles : ISlackFiles
    {
        public string filesdelete(filescommentsaddCl _filescommentsaddCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.delete?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filescommentsaddCl.file;           
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string filesinfo(filesinfoCl _filesinfoCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.delete?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filesinfoCl.file;
                if(_filesinfoCl.count != "")
                    ApiUrl += "&count=" + _filesinfoCl.count;
                if(_filesinfoCl.page != "")
                    ApiUrl += "&page=" + _filesinfoCl.page;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string fileslist(fileslistCl _fileslistCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.list?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_fileslistCl.user != "")
                ApiUrl += "&user=" + _fileslistCl.user;
                if (_fileslistCl.channel != "")
                    ApiUrl += "&channel=" + _fileslistCl.channel;
                if (_fileslistCl.ts_from != "")
                    ApiUrl += "&ts_from=" + _fileslistCl.ts_from;
                if (_fileslistCl.ts_to != "")
                    ApiUrl += "&ts_to=" + _fileslistCl.ts_to;
                if (_fileslistCl.types != "")
                    ApiUrl += "&types=" + _fileslistCl.types;
                if (_fileslistCl.count != "")
                    ApiUrl += "&count=" + _fileslistCl.count;
                if (_fileslistCl.page != "")
                    ApiUrl += "&page=" + _fileslistCl.page;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string filesrevokePublicURL(filesrevokePublicURLCl _filesrevokePublicURLCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.delete?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filesrevokePublicURLCl.file;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string filessharedPublicURL(filessharedPublicURLCl _filessharedPublicURLCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.sharedPublicURL?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filessharedPublicURLCl.file;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string filesupload(filesuploadCl _filesuploadCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.upload?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                if(_filesuploadCl.file != "")
                ApiUrl += "&file=" + _filesuploadCl.file;
                if (_filesuploadCl.content != "")
                ApiUrl += "&content=" + _filesuploadCl.content;
                if (_filesuploadCl.filetype != "")
                ApiUrl += "&filetype=" + _filesuploadCl.filetype;
                if (_filesuploadCl.filename != "")
                    ApiUrl += "&filename=" + _filesuploadCl.filename;
                if (_filesuploadCl.title != "")
                    ApiUrl += "&title=" + _filesuploadCl.title;
                if (_filesuploadCl.initial_comment != "")
                    ApiUrl += "&initial_comment=" + _filesuploadCl.initial_comment;
                if (_filesuploadCl.channels != "")
                    ApiUrl += "&channels=" + _filesuploadCl.channels;

                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
