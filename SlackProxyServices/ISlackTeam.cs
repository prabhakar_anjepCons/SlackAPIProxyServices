﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackTeam" in both code and config file together.
    [ServiceContract]
    public interface ISlackTeam
    {
        [OperationContract]
        string teamaccessLogs(teamaccessLogsCl _teamaccessLogsCl);
        [OperationContract]
        string teambillableInfo(teambillableInfoCl _teambillableInfoCl);
        [OperationContract]
        string teaminfo();
        [OperationContract]
        string teamintegrationLogs(teamintegrationLogsCl _teamintegrationLogsCl);
    }
    #region Model Declaration
    public class teamaccessLogsCl
    {
        [DataMember]
        public string before { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string page { get; set; }
    }
    public class teambillableInfoCl
    {
        [DataMember]
        public string user { get; set; }   
    }
    public class teamintegrationLogsCl
    {
        [DataMember]
        public string app_id { get; set; }
        [DataMember]
        public string change_type { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string page { get; set; }
        [DataMember]
        public string service_id { get; set; }
        [DataMember]
        public string user { get; set; }
    }
    #endregion
}
