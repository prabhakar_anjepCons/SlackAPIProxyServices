﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackUserGroupsUsers" in both code and config file together.
    [ServiceContract]
    public interface ISlackUserGroupsUsers
    {
        [OperationContract]
        string usergroupsuserslist(usergroupsuserslistCl _usergroupsuserslistCl);
        [OperationContract]
        string usergroupsusersupdate(usergroupsusersupdateCl _usergroupsusersupdateCl);
        //[OperationContract]
        //string usergroupsusers(usergroupsusersCl _usergroupsusersCl);
    }
    #region Model Declaration
    public class usergroupsuserslistCl
    {
        [DataMember(IsRequired = true)]
        public string usergroup { get; set; }
        [DataMember]
        public string include_disabled { get; set; }        
    }
    public class usergroupsusersupdateCl
    {
        [DataMember(IsRequired = true)]
        public string usergroup { get; set; }
        [DataMember]
        public string users { get; set; }
        [DataMember]
        public string include_count { get; set; }
    }
    public class usergroupsusersCl
    {
        [DataMember(IsRequired = true)]
        public string usergroup { get; set; }
        [DataMember]
        public string include_disabled { get; set; }
    }
    #endregion
}
