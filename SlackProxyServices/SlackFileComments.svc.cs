﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackFileComments" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackFileComments.svc or SlackFileComments.svc.cs at the Solution Explorer and start debugging.
    public class SlackFileComments : ISlackFileComments
    {
        public string filescommentsadd(filescommentsaddCl _filescommentsaddCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.comments.add?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filescommentsaddCl.file;
                ApiUrl += "&comment=" + _filescommentsaddCl.comment;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string filescommentsdelete(filescommentsdeleteCl _filescommentsdeleteCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.comments.delete?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filescommentsdeleteCl.file;
                ApiUrl += "&id=" + _filescommentsdeleteCl.id;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {   
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string filescommentsedit(filescommentseditCl _filescommentseditCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "files.comments.edit?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&file=" + _filescommentseditCl.file;
                ApiUrl += "&id=" + _filescommentseditCl.id;
                ApiUrl += "&comment=" + _filescommentseditCl.comment;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
