﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackgroups" in both code and config file together.
    [ServiceContract]
    public interface ISlackgroups
    {
        [OperationContract]
        string groupsarchive(groupsarchiveCl _groupsarchiveCl);

        [OperationContract]
        string groupsclose(groupscloseCl _groupscloseCl);

        [OperationContract]
        string groupscreate(groupscreateCl _groupscreateCl);

        [OperationContract]
        string groupscreateChild(groupscreateChildCl _groupscreateChildCl);

        [OperationContract]
        string groupshistory(groupshistorycl _groupshistorycl);

        [OperationContract]
        string groupsinfo(groupsinfocl _groupshistorycl);

        [OperationContract]
        string groupsinvite(groupsinvitecl _groupsinvitecl);
        [OperationContract]
        string groupskick(groupskickcl _groupskickcl);

        [OperationContract]
        string groupsleave(groupsleavecl _groupsleavecl);

        [OperationContract]
        string groupslist(groupslistcl _groupslistcl);

        [OperationContract]
        string groupsmark(groupsmarkcl _groupsmarkcl);

        [OperationContract]
        string groupsopen(groupsopencl _groupsopencl);

        [OperationContract]
        string groupsrename(groupsrenamecl _groupsrenamecl);

        [OperationContract]
        string groupsreplies(groupsrepliescl _groupsrepliescl);

        [OperationContract]
        string groupssetPurpose(groupssetPurposecl _groupssetPurposecl);

        [OperationContract]
        string groupsunarchive(groupsunarchivecl _groupsunarchivecl);

    }

    #region Model Declaration
    public class groupsarchiveCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
    }
    public class groupscloseCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
    }
    public class groupscreateCl
    {
        [DataMember(IsRequired = true)] 
        public string name { get; set; }
        [DataMember]
        public string validate { get; set; }
    }
    public class groupscreateChildCl
    {      
        [DataMember]
        public string channel { get; set; }
    }

    public class groupshistorycl
    {
        [DataMember]
        public string channel { get; set; }

        [DataMember]
        public string latest { get; set; }

        [DataMember]
        public string oldest { get; set; }

        [DataMember]
        public string inclusive { get; set; }

        [DataMember]
        public string count { get; set; }

        [DataMember]
        public string unreads { get; set; }
    }

    public class groupsinfocl
    {
        [DataMember]
        public string channel { get; set; }
    }
    public class groupsinvitecl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string user { get; set; }
    }
    public class groupskickcl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string user { get; set; }
    }

    public class groupsleavecl
    {
        [DataMember]
        public string channel { get; set; }      
    }

    public class groupslistcl
    {
        [DataMember]
        public string exclude_archived { get; set; }
    }
    public class groupsmarkcl
    {
        [DataMember]
        public string channel { get; set; }

        [DataMember]
        public string ts { get; set; }
    }

    public class groupsopencl
    {
        [DataMember]
        public string channel { get; set; }       
    }

    public class groupsrenamecl
    {
        [DataMember]
        public string channel { get; set; }

        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string validate { get; set; }
    }
    public class groupsrepliescl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string thread_ts { get; set; }        
    }
    public class groupssetPurposecl
    {
        [DataMember]
        public string channel { get; set; }
        [DataMember]
        public string purpose { get; set; }
    }

    public class groupsunarchivecl
    {
        [DataMember]
        public string channel { get; set; }      
    }
    #endregion
}
