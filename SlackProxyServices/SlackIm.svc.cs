﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackIm" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackIm.svc or SlackIm.svc.cs at the Solution Explorer and start debugging.
    public class SlackIm : ISlackIm
    {
        public string imclose(imcloseCl _imcloseCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "im.close?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _imcloseCl.channel;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string imhistory(imhistoryCl _imhistoryCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "im.history?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _imhistoryCl.channel;
                if(_imhistoryCl.count != "")
                    ApiUrl += "&count=" + _imhistoryCl.count;
                if(_imhistoryCl.inclusive != "")
                    ApiUrl += "&inclusive=" + _imhistoryCl.inclusive;
                if (_imhistoryCl.latest != "")
                    ApiUrl += "&latest=" + _imhistoryCl.latest;
                if (_imhistoryCl.oldest != "")
                    ApiUrl += "&oldest=" + _imhistoryCl.oldest;
                if (_imhistoryCl.unreads != "")
                    ApiUrl += "&unreads=" + _imhistoryCl.unreads;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string imlist()
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "im.list?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();               
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string immark(immarkCl _immarkCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "im.mark?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _immarkCl.channel;
                ApiUrl += "&ts=" + _immarkCl.ts;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string imopen(imopenCl _imopenCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "im.open?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&user=" + _imopenCl.user;
                if(_imopenCl.return_im != "")
                ApiUrl += "&return_im=" + _imopenCl.return_im;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string imreplies(imrepliesCl _imrepliesCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "im.replies?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&channel=" + _imrepliesCl.channel;
                if (_imrepliesCl.thread_ts != "")
                    ApiUrl += "&thread_ts=" + _imrepliesCl.thread_ts;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
