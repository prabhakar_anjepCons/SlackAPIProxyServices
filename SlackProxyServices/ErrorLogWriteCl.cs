﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SlackProxyServices
{
    public class ErrorLogWriteCl
    {
        public static void LogsWrite(string FileName, Exception ex, string Path)
        {
            using (StreamWriter writer = new StreamWriter(Path, true))
            {
                char c = '=';

                writer.WriteLine(DateTime.Now.ToShortDateString().PadRight(20, c) + DateTime.Now.ToShortTimeString().PadRight(20, c) + ex.Message.ToString().PadRight(20, c) + ex.StackTrace.ToString());
                writer.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------");
            }
        }
    }
}