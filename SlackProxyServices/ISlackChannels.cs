﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackChannels" in both code and config file together.
    [ServiceContract]
    public interface ISlackChannels
    {
        [OperationContract]
        string CreateChannels(CreatechannelCl _channelCl);

        [OperationContract]
        string channelsarchive(channelsarchiveCl _channelsarchiveCl);

        [OperationContract]
        string channelsinfo(CreatechannelCl _CreatechannelCl);

        [OperationContract]
        string channelsinvite(channelsinviteCl _channelsinviteCl);

        [OperationContract]
        string channelsjoin(CreatechannelCl _CreatechannelCl);

        [OperationContract]
        string channelskick(channelsinviteCl _channelsinviteCl);

        [OperationContract]
        string channelsleave(channelsarchiveCl _channelsarchiveCl);

        [OperationContract]
        string channelslist();

        [OperationContract]
        string channelsmark(channelsmarkCl _channelsmarkCl);

        [OperationContract]
        string channelsrename(channelsrenameCl _channelsrenameCl);

        [OperationContract]
        string channelsreplies(channelsrepliesCl _channelsrepliesCl);

        [OperationContract]
        string channelssetPurpose(channelssetPurposeCl _channelssetPurposeCl);

        [OperationContract]
        string channelssetTopic(channelssetTopicCl _channelssetTopicCl);

        [OperationContract]
        string channelsunarchive(channelsarchiveCl _channelsarchiveCl);
    }

    #region Model Declaration
    public class CreatechannelCl
    {
        [DataMember(IsRequired = true)]
        public string  name { get; set; }     
    }

    public class channelsarchiveCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
    }

    public class channelsinviteCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }

        [DataMember(IsRequired = true)]
        public string user { get; set; }
    }

    public class channelsmarkCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }

        [DataMember(IsRequired = true)]
        public string ts { get; set; }
    }

    public class channelsrenameCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }

        [DataMember(IsRequired = true)]
        public string name { get; set; }
    }
    public class channelsrepliesCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }

        [DataMember(IsRequired = true)]
        public string thread_ts { get; set; }
    }

    public class channelssetPurposeCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember(IsRequired = true)]
        public string purpose { get; set; }
    }

    public class channelssetTopicCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember(IsRequired = true)]
        public string topic { get; set; }
    }

    #endregion
}
