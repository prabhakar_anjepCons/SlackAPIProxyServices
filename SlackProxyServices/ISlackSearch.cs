﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackSearch" in both code and config file together.
    [ServiceContract]
    public interface ISlackSearch
    {
        [OperationContract]
        string searchall(SearchAllCl _SearchAllCl);
        [OperationContract]
        string searchfiles(searchfilesCl _searchfilesCl);
        [OperationContract]
        string searchmessages(searchmessagesCl _searchmessagesCl);
    }
    #region Model Declaration
    public class SearchAllCl
    {
        [DataMember]
        public string query { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string highlight { get; set; }
        [DataMember]
        public string page { get; set; }
        [DataMember]
        public string sort { get; set; }
        [DataMember]
        public string sort_dir { get; set; }
    }

    public class searchfilesCl
    {
        [DataMember]
        public string query { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string highlight { get; set; }
        [DataMember]
        public string page { get; set; }
        [DataMember]
        public string sort { get; set; }
        [DataMember]
        public string sort_dir { get; set; }
    }
    public class searchmessagesCl
    {
        [DataMember]
        public string query { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string highlight { get; set; }
        [DataMember]
        public string page { get; set; }
        [DataMember]
        public string sort { get; set; }
        [DataMember]
        public string sort_dir { get; set; }
    }
    #endregion
}
