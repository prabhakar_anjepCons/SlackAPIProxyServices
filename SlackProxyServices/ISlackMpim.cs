﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackMpim" in both code and config file together.
    [ServiceContract]
    public interface ISlackMpim
    {
        [OperationContract]
        string mpimclose(mpimcloseCl _mpimcloseCl);

        [OperationContract]
        string mpimhistory(mpimhistoryCl _mpimhistoryCl);

        [OperationContract]
        string mpimlist();
        [OperationContract]
        string mpimmark(mpimmarkCl _mpimmarkCl);
        [OperationContract]
        string mpimopen(mpimopenCl _mpimopenCl);
        [OperationContract]
        string mpimreplies(mpimrepliesCl _mpimrepliesCl);
        [OperationContract]
        string pinsadd(pinsaddCl _pinsaddCl);
        [OperationContract]
        string pinslist(pinslistCl _pinslistCl);
        [OperationContract]
        string pinsremove(pinsremoveCl _pinsremoveCl);
    }

    #region Model Declaration
    public class mpimcloseCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
    }
    public class mpimhistoryCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string count { get; set; }
        [DataMember]
        public string inclusive { get; set; }
        [DataMember]
        public string latest { get; set; }
        [DataMember]
        public string oldest { get; set; }
        [DataMember]
        public string unreads { get; set; }
    }
    public class mpimmarkCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string ts { get; set; }
    }
    public class mpimopenCl
    {
        [DataMember(IsRequired = true)]
        public string users { get; set; }        
    }
    public class mpimrepliesCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string thread_ts { get; set; }
    }
    public class pinsaddCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    public class pinslistCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }       
    }
    public class pinsremoveCl
    {
        [DataMember(IsRequired = true)]
        public string channel { get; set; }
        [DataMember]
        public string file { get; set; }
        [DataMember]
        public string file_comment { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    #endregion
}
