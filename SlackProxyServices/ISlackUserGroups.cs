﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackUserGroups" in both code and config file together.
    [ServiceContract]
    public interface ISlackUserGroups
    {
        [OperationContract]
        string usergroupscreate(usergroupscreateCl _usergroupscreateCl);
        [OperationContract]
        string usergroupsdisable(usergroupsdisableCl _usergroupsdisableCl);
        [OperationContract]
        string usergroupsenable(usergroupsenableCl _usergroupsenableCl);
        [OperationContract]
        string usergroupslist(usergroupslistCl _usergroupslistCl);
        [OperationContract]
        string usergroupsupdate(usergroupsupdateCl _usergroupsupdateCl);
    }
    #region Model Declaration
    public class usergroupscreateCl
    {
        [DataMember(IsRequired = true)]
        public string name { get; set; }
        [DataMember]
        public string channels { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string handle { get; set; }
        [DataMember]
        public string include_count { get; set; }
    }
    public class usergroupsdisableCl
    {
        [DataMember(IsRequired = true)]
        public string usergroup { get; set; }
        [DataMember]
        public string include_count { get; set; }
    }
    public class usergroupsenableCl
    {
        [DataMember(IsRequired = true)]
        public string usergroup { get; set; }
        [DataMember]
        public string include_count { get; set; }
    }
    public class usergroupslistCl
    {
        [DataMember(IsRequired = true)]
        public string include_count { get; set; }
        [DataMember]
        public string include_disabled { get; set; }
        [DataMember]
        public string include_users { get; set; }
    }
    public class usergroupsupdateCl
    {
        [DataMember(IsRequired = true)]
        public string usergroup { get; set; }
        [DataMember]
        public string channels { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string handle { get; set; }
        [DataMember]
        public string include_count { get; set; }
        [DataMember]
        public string name { get; set; }
    }
    #endregion
}
