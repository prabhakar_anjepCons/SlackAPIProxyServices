﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SlackUserGroups" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SlackUserGroups.svc or SlackUserGroups.svc.cs at the Solution Explorer and start debugging.
    public class SlackUserGroups : ISlackUserGroups
    {
        public string usergroupscreate(usergroupscreateCl _usergroupscreateCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.create?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&name=" + _usergroupscreateCl.name;
                if(_usergroupscreateCl.channels != "")
                ApiUrl += "&channels=" + _usergroupscreateCl.channels;
                if (_usergroupscreateCl.description != "")
                    ApiUrl += "&description=" + _usergroupscreateCl.description;
                if (_usergroupscreateCl.handle != "")
                    ApiUrl += "&handle=" + _usergroupscreateCl.handle;
                if (_usergroupscreateCl.include_count != "")
                    ApiUrl += "&include_count=" + _usergroupscreateCl.include_count;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string usergroupsdisable(usergroupsdisableCl _usergroupsdisableCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.disable?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&usergroup=" + _usergroupsdisableCl.usergroup;
                if(_usergroupsdisableCl.include_count != "")
                    ApiUrl += "&usergroup=" + _usergroupsdisableCl.include_count;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string usergroupsenable(usergroupsenableCl _usergroupsenableCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.enable?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&usergroup=" + _usergroupsenableCl.usergroup;
                if (_usergroupsenableCl.include_count != "")
                    ApiUrl += "&usergroup=" + _usergroupsenableCl.include_count;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
        public string usergroupslist(usergroupslistCl _usergroupslistCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.list?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();             
                if (_usergroupslistCl.include_count != "")
                    ApiUrl += "&include_count=" + _usergroupslistCl.include_count;
                if (_usergroupslistCl.include_disabled != "")
                    ApiUrl += "&include_disabled=" + _usergroupslistCl.include_disabled;
                if (_usergroupslistCl.include_users != "")
                    ApiUrl += "&include_users=" + _usergroupslistCl.include_users;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }

        public string usergroupsupdate(usergroupsupdateCl _usergroupsupdateCl)
        {
            string responseFromServer = string.Empty;
            try
            {
                string ApiUrl = string.Empty;
                ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
                ApiUrl += "usergroups.enable?";
                ApiUrl += "token=" + ConfigurationManager.AppSettings["TokenKey"].ToString();
                ApiUrl += "&usergroup=" + _usergroupsupdateCl.usergroup;
                if (_usergroupsupdateCl.channels != "")
                    ApiUrl += "&channels=" + _usergroupsupdateCl.channels;
                if (_usergroupsupdateCl.description != "")
                    ApiUrl += "&description=" + _usergroupsupdateCl.description;
                if (_usergroupsupdateCl.handle != "")
                    ApiUrl += "&handle=" + _usergroupsupdateCl.handle;
                if (_usergroupsupdateCl.include_count != "")
                    ApiUrl += "&include_count=" + _usergroupsupdateCl.include_count;
                if (_usergroupsupdateCl.name != "")
                    ApiUrl += "&name=" + _usergroupsupdateCl.name;
                ApiUrl += "&pretty=1";
                responseFromServer = WebRequestCl.ReturnWebRequest(ApiUrl);
            }
            catch (Exception ex)
            {
                string fileName = "err.txt";
                string filePath = string.Format("{0}Data\\{1}", HttpContext.Current.Request.PhysicalApplicationPath, fileName);
                ErrorLogWriteCl.LogsWrite("err", ex, filePath);
            }
            return responseFromServer;
        }
    }
}
