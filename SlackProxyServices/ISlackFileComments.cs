﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SlackProxyServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISlackFileComments" in both code and config file together.
    [ServiceContract]
    public interface ISlackFileComments
    {
        [OperationContract]
        string filescommentsadd(filescommentsaddCl _filescommentsaddCl);

        [OperationContract]
        string filescommentsdelete(filescommentsdeleteCl _filescommentsdeleteCl);

        [OperationContract]
        string filescommentsedit(filescommentseditCl _filescommentseditCl);
    }

    #region Model Declaration
    public class filescommentsaddCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }
        [DataMember(IsRequired = true)]
        public string comment { get; set; }
    }

    public class filescommentsdeleteCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }
        [DataMember(IsRequired = true)]
        public string id { get; set; }
    }

    public class filescommentseditCl
    {
        [DataMember(IsRequired = true)]
        public string file { get; set; }
        [DataMember(IsRequired = true)]
        public string id { get; set; }
        [DataMember(IsRequired = true)]
        public string comment { get; set; }
    }
    #endregion
}
